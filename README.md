# Flysystem Sharepoint storage

A Sharepoint Storage filesystem for [Flysystem](https://flysystem.thephpleague.com/docs/).

This package contains a Flysystem adapter for SharePoint.

## Installation

```bash
composer require cadix/flysystem-sharepoint-adapter
```

## Usage
```php
// Create a Sharepoint Client to talk with the API
$client = new Client('client-id', 'client-secret', 'branch', 'tenant-prefix');
   
// Create the Adapter that implements Flysystems AdapterInterface
$adapter = new SharePointAdapter($client);

// Create FileSystem
$filesystem = new Filesystem($adapter);

// see http://flysystem.thephpleague.com/api/ for full list of available functionality
```
    
## SharePoint app

The adapter only works with a SharePoint app registered to tenant.
This [documentation](https://docs.microsoft.com/en-us/sharepoint/dev/solution-guidance/security-apponly-azureacs) will guide you trough the process.

### Set up the app
Go to `https://<your-tenant>.sharepoint.com/_layouts/15/appregnew.aspx`. 
And hit the `generate` buttons. 
This will give you your `client-id` and `client-secret`.

### Set the app permissions
Go to `https://<your-tenant>-admin.sharepoint.com/_layouts/15/appinv.aspx`.  
As the `app id` use the `client id` generated in the previous step.

For the permissions use:
```xml
<AppPermissionRequests AllowAppOnlyPolicy="true">
  <AppPermissionRequest Scope="http://sharepoint/content/tenant" Right="FullControl" />
</AppPermissionRequests>
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Contributions are **welcome** and will be fully **credited**. 
We accept contributions via Pull Requests on [GitLab](https://gitlab.com/cadix/web/flysystem-sharepoint-adapter).

### Pull Requests

- **[PSR-2 Coding Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)** - The easiest way to apply the conventions is run `composer cs-fixer`.
- **Document any change in behaviour** - Make sure the `README.md` and any other relevant documentation are kept up-to-date.
- **Create feature branches** - Don't ask us to pull from your master branch.
- **One pull request per feature** - If you want to do more than one thing, send multiple pull requests.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Reference

- [SharePoint Icon](https://developer.microsoft.com/en-us/fluentui#/styles/web/office-brand-icons) is from Microsoft;

# About Cadix
With a team of passionate professionals [Cadix](https://www.cadix.nl) delivers solutions for designing, managing and sharing of information in construction, mechanical engineering and infra.
We realize this with our trainings, consultancy and secondment.
With the extensive knowledge of our professionals we are able to find and realize a suitable solution for you. 

