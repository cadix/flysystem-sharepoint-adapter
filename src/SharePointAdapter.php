<?php

namespace Cadix\FlysystemSharePoint;

use Exception;
use League\Flysystem\Config;
use League\Flysystem\DirectoryAttributes;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\UnableToCopyFile;
use League\Flysystem\UnableToCreateDirectory;
use League\Flysystem\UnableToDeleteDirectory;
use League\Flysystem\UnableToDeleteFile;
use League\Flysystem\UnableToMoveFile;
use League\Flysystem\UnableToReadFile;
use League\Flysystem\UnableToRetrieveMetadata;
use League\Flysystem\UnableToWriteFile;
use League\MimeTypeDetection\ExtensionMimeTypeDetector;

/**
 * Class SharePointAdapter
 *
 * @package Cadix\FlysystemSharePoint
 */
class SharePointAdapter implements FilesystemAdapter
{
    /**
     * SharePointAdapter constructor.
     *
     * @param Client $client
     * @param string $prefix
     * @param bool   $debug
     */
    public function __construct(protected Client $client, protected string $prefix = '', protected bool $debug = false)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function write(string $path, string $contents, Config $config): void
    {
        try {
            $this->client->upload($this->applyPathPrefix($path), $contents);
        } catch (Exception $exception) {
            $this->failed(UnableToWriteFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function writeStream(string $path, $contents, Config $config): void
    {
        try {
            $this->client->uploadStream($this->applyPathPrefix($path), $contents);
        } catch (Exception $exception) {
            $this->failed(UnableToWriteFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function copy(string $source, string $destination, Config $config): void
    {
        try {
            $this->client->copyFile($this->applyPathPrefix($source), $this->applyPathPrefix($destination));
        } catch (Exception $exception) {
            $this->failed(UnableToCopyFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function move(string $source, string $destination, Config $config): void
    {
        try {
            $this->client->moveFile($this->applyPathPrefix($source), $this->applyPathPrefix($destination));
        } catch (Exception $exception) {
            $this->failed(UnableToMoveFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function delete(string $path): void
    {
        try {
            $this->client->deleteFile($this->applyPathPrefix($path));
        } catch (Exception $exception) {
            $this->failed(UnableToDeleteFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteDirectory(string $path): void
    {
        try {
            $this->client->deleteDirectory($this->applyPathPrefix($path));
        } catch (Exception $exception) {
            $this->failed(UnableToDeleteDirectory::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function createDirectory(string $path, Config $config): void
    {
        try {
            $this->client->createDirectory($this->applyPathPrefix($path));
        } catch (Exception $exception) {
            $this->failed(UnableToCreateDirectory::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function directoryExists(string $path): bool
    {
        return $this->client->directoryExists($this->applyPathPrefix($path));
    }

    /**
     * {@inheritDoc}
     */
    public function fileExists(string $path): bool
    {
        return $this->client->fileExists($this->client->getRelativeUrl($this->applyPathPrefix($path)));
    }

    /**
     * {@inheritDoc}
     */
    public function read(string $path): string
    {
        $contents = '';

        try {
            $contents = $this->client->readFile($this->applyPathPrefix($path));
        } catch (Exception $exception) {
            $this->failed(UnableToReadFile::class, $exception);
        }

        return $contents;
    }

    /**
     * {@inheritDoc}
     */
    public function readStream(string $path)
    {
        try {
            $stream = fopen('php://temp', 'rb+');
            fwrite($stream, $this->read($path));
            rewind($stream);

            if (! is_resource($stream)) {
                return false;
            }

            return $stream;
        } catch (Exception $exception) {
            $this->failed(UnableToReadFile::class, $exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function listContents(string $path, bool $deep): iterable
    {
        $contents = $this->client->listContents($this->applyPathPrefix($path), $deep);

        foreach ($contents as $type => $content) {
            foreach ($content as $path) {
                yield $this->getMetadata($path, $type);
            }
        }
    }

    public function getMetadata(string $path, string $type = 'file'): FileAttributes|DirectoryAttributes|bool
    {
        $file = null;

        try {
            $file = match ($type) {
                'file', 'files'            => $this->client->getFileMetaData($this->applyPathPrefix($path)),
                'directory', 'directories' => $path
            };
        } catch (Exception $exception) {
            echo 2;
            $this->failed(UnableToRetrieveMetadata::class, $exception);
        }

        if (! $file) {
            return false;
        }

        return match ($type) {
            'file', 'files'            => new FileAttributes(
                $file->getProperty('ServerRelativeUrl'),
                $file->getProperty('Length'),
                $file->getProperty('Level'),
                strtotime($file->getProperty('TimeLastModified')) - time(),
                ( new ExtensionMimeTypeDetector() )->detectMimetypeFromFile($path),
                [
                    'name' => $file->getProperty('Name'),
                ]
            ),
            'directory', 'directories' => new DirectoryAttributes($file)
        };
    }

    /**
     * {@inheritDoc}
     */
    public function fileSize(string $path): FileAttributes
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritDoc}
     */
    public function mimeType(string $path): FileAttributes
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritDoc}
     */
    public function lastModified(string $path): FileAttributes
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritDoc}
     */
    public function setVisibility(string $path, string $visibility): void
    {
        // TODO: Implement setVisibility() method.
    }

    /**
     * {@inheritDoc}
     */
    public function visibility(string $path): FileAttributes
    {
        return $this->getMetadata($path);
    }

    /**
     * @param string    $exception_class
     * @param Exception $exception
     * @param bool      $return
     *
     * @return bool
     */
    public function failed(string $exception_class, Exception $exception, bool $return = false): void
    {
        if ($this->isDebugEnabled()) {
            throw new $exception_class($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    public function isDebugEnabled(): bool
    {
        return $this->debug;
    }

    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    /**
     * A Laravel specific method
     *
     * @param string $path
     * @return string
     */
    public function getUrl(string $path): string
    {
        return $this->client->getRelativeUrl($this->applyPathPrefix($path));
    }

    public function applyPathPrefix(string $path): string
    {
        if (! str_contains($path, $this->prefix)) {
            $path = $this->prefix . '/' . $path;
        }

        return $path;
    }
}
