<?php

namespace Cadix\FlysystemSharePoint;

use Office365\Runtime\Auth\ClientCredential;
use Office365\Runtime\Http\RequestException;
use Office365\SharePoint\ClientContext;
use Office365\SharePoint\File;
use Office365\SharePoint\Folder;
use Office365\SharePoint\MoveOperations;

/**
 * Class Client
 *
 * @package Cadix\FlysystemSharePoint
 */
class Client
{
    protected ClientContext $context;

    /**
     * Client constructor.
     *
     * @param string $client_id
     * @param string $client_secret
     * @param string $branch
     * @param string $tenant
     */
    public function __construct(
        protected string $client_id,
        protected string $client_secret,
        protected string $branch,
        protected string $tenant
    ) {
        $credentials = new ClientCredential($this->client_id, $this->client_secret);
        $url = sprintf(
            'https://%s.sharepoint.com/%s',
            $this->tenant,
            $this->branch
        );
        $this->context = ( new ClientContext($url) )->withCredentials($credentials);
    }

    /**
     * @param string $path
     *
     * @return array
     */
    public function listFiles(string $path): array
    {
        $files = [];

        $rootFolder = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl($this->getRelativeUrl($path))
            ->getFiles()
            ->get()
            ->executeQuery();

        foreach ($rootFolder->getData() as $file) {
            $files[] = $file->getServerRelativeUrl();
        }

        return $files;
    }

    public function listContents(string $path, bool $recursive = true): array
    {
        $rootFolder = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl($this->getRelativeUrl($path));

        $contents = [];

        return $this->forEachFile($contents, $rootFolder, $recursive);
    }

    private function forEachFile($contents, Folder $parentFolder, bool $recursive = false)
    {
        $files = $parentFolder->getFiles()->get()->executeQuery();
        $folders = $parentFolder->getFolders()->get()->executeQuery();

        foreach ($files as $file) {
            $contents['files'][] = $file->getServerRelativeUrl();
        }

        foreach ($folders->getData() as $folder) {
            if ($recursive) {
                $contents = $this->forEachFile($contents, $folder, $recursive);
            }

            $contents['directories'][] = $folder->getServerRelativeUrl();
        }

        return $contents;
    }

    public function readFile(string $path): string
    {
        return File::openBinary($this->context, $this->getRelativeUrl($path));
    }

    public function listDirectories(string $path): array
    {
        $directories = [];
        $rootFolder = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl($this->getRelativeUrl($path))
            ->getFolders()
            ->get()
            ->executeQuery();

        foreach ($rootFolder->getData() as $folder) {
            $directories[] = $folder->getServerRelativeUrl();
        }

        return $directories;
    }

    /**
     * @param string $path
     * @param string $contents
     *
     * @return string
     */
    public function upload(string $path, string $contents): string
    {
        $path = $this->getRelativeUrl($path);

        $upload_file = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl(dirname($path))
            ->uploadFile(basename($path), $contents);

        $this->context->executeQuery();

        return $upload_file->getServerRelativeUrl();
    }

    /**
     * @param string $path
     * @param $resource
     *
     * @return string
     */
    public function uploadStream(string $path, $resource): string
    {
        if (! is_resource($resource)) {
            throw new \InvalidArgumentException(sprintf(
                'Argument must be a valid resource type. %s given.',
                gettype($resource)
            ));
        }

        return $this->upload($this->getRelativeUrl($path), stream_get_contents($resource));
    }

    public function createDirectory(string $path, string|null $dirname = null): bool
    {
        $path = $this->getRelativeUrl($path);
        if (! $dirname) {
            $directories = explode('/', $path);
            $dirname = end($directories);
            $path = str_replace('/'.$dirname, '', $path);
        }

        $rootFolder = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl($path);

        $newFolder = $rootFolder
            ->getFolders()
            ->add($dirname)
            ->executeQuery();

        return (bool)$newFolder->getServerRelativeUrl();
    }

    public function deleteDirectory(string $path): bool
    {
        $path = $this->getRelativeUrl($path);
        $folderToDelete = $this->context
            ->getWeb()
            ->getFolderByServerRelativeUrl($path);
        $folderToDelete->deleteObject();

        $this->context->executeQuery();

        return ! $this->directoryExists($path);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function deleteFile(string $path): bool
    {
        $path = $this->getRelativeUrl($path);
        $fileToDelete = $this->context
            ->getWeb()
            ->getFileByServerRelativeUrl($path);
        $fileToDelete->deleteObject();

        $this->context->executeQuery();

        return ! $this->fileExists($path);
    }

    public function fileExists(string $path): bool
    {
        $path = $this->getRelativeUrl($path);

        try {
            $this->context
                ->getWeb()
                ->getFileByServerRelativeUrl($path)
                ->get()
                ->executeQuery();
        } catch (RequestException $exception) {
            if ($exception->getCode() === 404) {
                return false;
            }

            throw $exception;
        }

        return true;
    }

    public function directoryExists(string $path, string|null $directoryName = null): bool
    {
        $path = $this->getRelativeUrl($path);

        try {
            if ($directoryName) {
                $path = $path.'/'.$directoryName;
            }

            $this->context
                ->getWeb()
                ->getFolderByServerRelativeUrl($path)
                ->get()
                ->executeQuery();
        } catch (RequestException $exception) {
            if ($exception->getCode() === 404) {
                return false;
            }

            throw $exception;
        }

        return true;
    }

    /**
     * @param string $path
     *
     * @return File
     */
    public function getFileMetaData(string $path): File
    {
        $file = $this->context
            ->getWeb()
            ->getFileByServerRelativeUrl($this->getRelativeUrl($path))
            ->expand('ListItemAllFields');

        $this->context->load($file);
        $this->context->executeQuery();

        return $file;
    }

    public function renameFile(string $path, string $newPath): bool
    {
        $path = $this->getRelativeUrl($path);
        $newPath = $this->getRelativeUrl($newPath);

        $sourceFile = $this->context
            ->getWeb()
            ->getFileByServerRelativeUrl($path)
            ->expand('ListItemAllFields');

        $this->context->load($sourceFile);

        $targetFile = $sourceFile->moveTo($newPath, MoveOperations::Overwrite);

        $this->context->executeQuery();

        return ! $this->fileExists($path) && $this->fileExists($newPath);
    }

    public function copyFile(string $path, string $newPath): bool
    {
        $path = $this->getRelativeUrl($path);
        $newPath = $this->getRelativeUrl($newPath);

        $sourceFile = $this->context
            ->getWeb()
            ->getFileByServerRelativeUrl($path);

        $this->context->load($sourceFile);

        $targetFile = $sourceFile->copyTo($newPath, true);

        $this->context->executeQuery();

        return $this->fileExists($newPath);
    }

    public function moveFile(string $path, string $newPath): bool
    {
        $path = $this->getRelativeUrl($path);
        $newPath = $this->getRelativeUrl($newPath);
        if ($this->fileExists($path)) {
            $sourceFile = $this->context
                ->getWeb()
                ->getFileByServerRelativeUrl($path);

            $this->context->load($sourceFile);

            $targetFile = $sourceFile->moveTo($newPath, MoveOperations::Overwrite);

            $this->context->executeQuery();

            return ! $this->fileExists($path) && $this->fileExists($newPath);
        }

        if ($this->directoryExists($path)) {
            $sourceFolder = $this->context
                ->getWeb()
                ->getFolderByServerRelativeUrl($path);

            $this->context->load($sourceFolder);

            $targetFolder = $sourceFolder->moveTo($newPath, MoveOperations::Overwrite);

            $this->context->executeQuery();

            return ! $this->directoryExists($path) && $this->directoryExists($newPath);
        }
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->client_id;
    }

    /**
     * @param string $client_id
     */
    public function setClientId(string $client_id): void
    {
        $this->client_id = $client_id;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    /**
     * @param string $client_secret
     */
    public function setClientSecret(string $client_secret): void
    {
        $this->client_secret = $client_secret;
    }

    /**
     * @return string
     */
    public function getBranch(): string
    {
        return $this->branch;
    }

    /**
     * @param string $branch
     */
    public function setBranch(string $branch): void
    {
        $this->branch = $branch;
    }

    public function getTenant(): string
    {
        return $this->tenant;
    }

    public function setTenant(string $tenant): void
    {
        $this->tenant = $tenant;
    }

    public function getRelativeUrl(string $path): string
    {
        if (! str_contains($path, $this->branch)) {
            return '/' . $this->branch . '/' . $path;
        }

        if (str_contains($path, $this->branch) && $path[0] !== '/') {
            return '/'.$path;
        }

        return $path;
    }
}
