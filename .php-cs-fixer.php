<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    ->name('*.php')
    ->exclude('/vendor');

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@PSR12'                                      => true,
    '@PHP80Migration'                             => true,
    '@PSR2'                                       => true,

    // Alias
    'no_mixed_echo_print'                         => [
        'use' => 'echo',
    ],

    // Array notation
    'array_syntax'                                => ['syntax' => 'short'],
    'no_multiline_whitespace_around_double_arrow' => true,
    'no_trailing_comma_in_singleline_array'       => true,
    'trim_array_spaces'                           => true,

    // Basic
    'braces'                                      => [
        'allow_single_line_anonymous_class_with_empty_body' => false,
        'allow_single_line_closure'                         => false,
        'position_after_functions_and_oop_constructs'       => 'next',
        'position_after_control_structures'                 => 'same',
        'position_after_anonymous_constructs'               => 'same',
    ],
    'encoding'                                    => true,

    // Casing
    'native_function_type_declaration_casing'     => true,
    'no_short_bool_cast'                          => true,

    // Class Notation
    'single_trait_insert_per_statement'           => true,
    'single_class_element_per_statement'          => true,

    // Comment
    'no_empty_comment'                            => true,

    // Control Structure
    'trailing_comma_in_multiline'                 => true,

    // Function Notation
    'function_typehint_space'                     => true,
    'return_type_declaration'                     => [
        'space_before' => 'none',
    ],

    // Import
    'fully_qualified_strict_types'                => true,
    'no_unused_imports'                           => true,
    'ordered_imports'                             => [
        'sort_algorithm' => 'alpha',
    ],
    'single_import_per_statement'                 => true,

    // PHP tag
    'linebreak_after_opening_tag'                 => true,

    // PHPDoc
    'align_multiline_comment'                     => true,
    'no_blank_lines_after_phpdoc'                 => true,
    'no_empty_phpdoc'                             => true,
    'phpdoc_add_missing_param_annotation'         => true,
    'phpdoc_align'                                => true,
    'phpdoc_annotation_without_dot'               => true,
    'phpdoc_indent'                               => true,
    'phpdoc_separation'                           => true,
    'phpdoc_single_line_var_spacing'              => true,
    'phpdoc_scalar'                               => true,
    'phpdoc_trim'                                 => true,
    'phpdoc_var_without_name'                     => true,

    // PHPUnit
    'php_unit_method_casing'                      => [
        'case' => 'snake_case',
    ],

    'class_attributes_separation'       => [
        'elements' => [
            'method' => 'one',
        ],
    ],
    'method_argument_space'             => [
        'on_multiline'                     => 'ensure_fully_multiline',
        'keep_multiple_spaces_after_comma' => true,
    ],

    // Operator
    'binary_operator_spaces'            => [
        'operators' => [
            '=>' => 'align',
        ],
    ],
    'not_operator_with_successor_space' => true,
    'unary_operator_spaces'             => true,

    // Return
    'no_useless_return'                 => true,

    // Semicolon
    'no_empty_statement'                => true,

    // Whitespace
    'array_indentation'                 => true,
    'blank_line_before_statement'       => [
        'statements' => ['break', 'continue', 'declare', 'return', 'throw', 'try'],
    ],
    'method_chaining_indentation'       => true,
    'no_spaces_around_offset'           => [
        'positions' => [
            'outside',
        ],
    ],
    'no_spaces_inside_parenthesis'      => true,
    'no_whitespace_in_blank_line'       => true,
])
    ->setFinder($finder);
