<?php

namespace Cadix\FlysystemSharePoint\Tests;

use Cadix\FlysystemSharePoint\Client;

class ClientTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_instantiated(): void
    {
        $this->assertInstanceOf(Client::class, $this->getClientInstance());
    }

    /**
     * @test
     */
    public function it_can_list_files_in_a_directory(): void
    {
        $path = $this->config['prefix'].'/testing.txt';

        $this->assertIsArray($this->getClientInstance()->listFiles($path));
    }

    /**
     * @test
     */
    public function it_can_read_a_file(): void
    {
        $fileContent = '# Testing create';
        $file = $this->config['prefix'].'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, $fileContent);

        $download = $this->getClientInstance()->readFile($file);

        $this->assertSame($fileContent, $download);
    }

    public function it_can_read_a_file_from_stream(): void
    {
        $fileContent = '# Testing create';
        $file = $this->config['prefix'].'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, $fileContent);

        $download = $this->getClientInstance()->readFileStream($file);
    }

    /**
     * @test
     */
    public function it_can_list_directories_in_a_directory(): void
    {
        $path = $this->config['prefix'];

        $this->assertIsArray($this->getClientInstance()->listDirectories($path));
    }

    /**
     * @test
     */
    public function it_can_upload_files(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, '# Testing create');

        $this->assertSame('/'.$this->config['branch'].'/'.$this->config['prefix'].'/testing.txt', $upload);
    }

    /**
     * @test
     */
    public function it_can_create_new_folder(): void
    {
        $folder = 'test';
        $path = $this->config['prefix'];

        $created = $this->getClientInstance()->createDirectory($path, $folder);

        $this->assertTrue($created);
    }

    /**
     * @test
     */
    public function it_can_delete_files(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $deleted = $this->getClientInstance()->deleteFile($file);

        $this->assertTrue($deleted);
        $this->assertFalse($this->getClientInstance()->fileExists($this->getClientInstance()->getRelativeUrl($file)));
    }

    /**
     * @test
     */
    public function it_can_check_if_file_exists(): void
    {
        $file = $this->config['prefix'].'/testing.txt';

        $upload = $this->getClientInstance()->upload($file, '# Testing create');
        $this->assertTrue($this->getClientInstance()->fileExists($this->getClientInstance()->getRelativeUrl($file)));


        $deleted = $this->getClientInstance()->deleteFile($file);
        $this->assertFalse($this->getClientInstance()->fileExists($this->getClientInstance()->getRelativeUrl($file)));
    }

    /**
     * @test
     */
    public function it_can_check_if_the_directory_exists(): void
    {
        $path = $this->config['prefix'];
        $directoryName = 'test';

        $created = $this->getClientInstance()->createDirectory($path, $directoryName);
        $relative_url = $this->getClientInstance()->getRelativeUrl($path);
        $this->assertTrue($this->getClientInstance()->directoryExists($relative_url, $directoryName));

        $deleted = $this->getClientInstance()->deleteDirectory($path.'/'.$directoryName);
        $this->assertFalse($this->getClientInstance()->directoryExists($relative_url, $directoryName));
    }

    /**
     * @test
     */
    public function it_can_get_a_file_with_metadata(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, '# Testing create');

        $metadata = $this->getClientInstance()->getFileMetaData($file);

        $this->assertSame('testing.txt', $metadata->getProperty('Name'));
    }

    /**
     * @test
     */
    public function it_can_rename_a_file(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $renamedFile = $this->config['prefix'].'/testing_renamed.txt';
        $upload = $this->getClientInstance()->upload($file, '# Testing create');

        $rename = $this->getClientInstance()->renameFile($file, $renamedFile);

        $this->assertTrue($rename);
    }

    /**
     * @test
     */
    public function it_can_copy_a_file(): void
    {
        $file = $this->config['prefix'].'/testing_renamed.txt';
        $copyFile = $this->config['prefix'].'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, '# Testing create');

        $copy = $this->getClientInstance()->copyFile($file, $copyFile);

        $this->assertTrue($copy);
    }

    /**
     * @test
     */
    public function it_can_move_a_file(): void
    {
        $path = $this->config['prefix'];
        $testDirectory = 'test';
        $file = $path.'/testing_renamed.txt';
        $moveFile = $path.'/'.$testDirectory.'/testing.txt';
        $upload = $this->getClientInstance()->upload($file, '# Testing create');

        $this->getClientInstance()->createDirectory($path, $testDirectory);
        $move = $this->getClientInstance()->moveFile($file, $moveFile);

        $this->assertTrue($move);
    }

    /**
     * @test
     */
    public function it_can_change_the_branch(): void
    {
        $client = new Client(
            1234,
            'secret',
            'branch',
            'tenant'
        );

        $client->setBranch('dev');

        $this->assertEquals('dev', $client->getBranch());
    }

    /**
     * @test
     */
    public function it_can_change_the_client_id(): void
    {
        $client = new Client(
            1234,
            'secret',
            'branch',
            'tenant'
        );
        $client->setClientId('12345678');

        $this->assertEquals('12345678', $client->getClientId());
    }

    /**
     * @test
     */
    public function it_can_change_the_client_secret(): void
    {
        $client = new Client(
            1234,
            'secret',
            'branch',
            'tenant'
        );
        $client->setClientSecret('12345678');

        $this->assertEquals('12345678', $client->getClientSecret());
    }

    /**
     * @test
     */
    public function it_can_change_the_tenant(): void
    {
        $client = new Client(
            1234,
            'secret',
            'branch',
            'tenant'
        );
        $client->setTenant('flysystem');

        $this->assertEquals('flysystem', $client->getTenant());
    }

    /**
     * @test
     */
    public function it_can_generate_a_relative_url(): void
    {
        $path = $this->config['prefix'].'/testing.txt';

        $this->assertSame('/'.$this->config['branch'].'/'.$path, $this->getClientInstance()->getRelativeUrl($path));
    }
}
