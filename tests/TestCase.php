<?php

namespace Cadix\FlysystemSharePoint\Tests;

use Cadix\FlysystemSharePoint\Client;
use Cadix\FlysystemSharePoint\SharePointAdapter;
use PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    public array $config;

    protected function setUp(): void
    {
        parent::setUp();

        $this->config = require(__DIR__.'/config/config.testing.php');
    }

    public function getClientInstance(): Client
    {
        return new Client(
            $this->config[ 'client_id' ],
            $this->config[ 'client_secret' ],
            $this->config[ 'branch' ],
            $this->config[ 'tenant' ]
        );
    }

    public function getAdapterInstance(): SharePointAdapter
    {
        return new SharePointAdapter($this->getClientInstance());
    }
}
