<?php

namespace Cadix\FlysystemSharePoint\Tests;

use Cadix\FlysystemSharePoint\Client;
use Cadix\FlysystemSharePoint\SharePointAdapter;
use League\Flysystem\AdapterTestUtilities\FilesystemAdapterTestCase;
use League\Flysystem\Config;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;

class SharePointAdapterTest extends FilesystemAdapterTestCase
{
    public array $config;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public static function createFilesystemAdapter(): FilesystemAdapter
    {
        return new SharePointAdapter(self::getClientInstance());
    }

    protected static function getClientInstance(): Client
    {
        $config = require(__DIR__.'/config/config.testing.php');

        return new Client(
            $config[ 'client_id' ],
            $config[ 'client_secret' ],
            $config[ 'branch' ],
            $config[ 'tenant' ]
        );
    }

    /**
     * @test
     */
    public function it_can_retrieve_client_instance(): void
    {
        $this->assertInstanceOf(Client::class, $this->adapter()->getClient());
    }

    /**
     * @test
     */
    public function it_can_set_client_instance(): void
    {
        $this->setInvalidClientId();

        $this->assertEquals($this->adapter()->getClient()
            ->getClientId(), '123');

        $this->restoreClientId();
    }

    /**
     * @test
     */
    public function it_can_write_a_file(): void
    {
        $this->adapter()->write($this->config['prefix'].'/testing.txt', '# Testing create', new Config());

        $this->assertTrue($this->adapter()->fileExists($this->config['prefix'].'/testing.txt'));
    }

    /**
     * @test
     */
    public function it_can_write_a_file_stream(): void
    {
        $stream = fopen(__DIR__.'/assets/testing.txt', 'r+');

        $path = $this->config['prefix'].'/testing.txt';
        $this->adapter()->writeStream($path, $stream, new Config());

        fclose($stream);

        $this->assertTrue($this->adapter()->fileExists($this->config['prefix'].'/testing.txt'));

        // Clean up
        $this->adapter()->delete($path);
    }

    /**
     * @test
     */
    public function it_can_copy_a_file(): void
    {
        $this->adapter()->write($this->config['prefix'].'/testing_renamed.txt', '# Testing create', new Config());
        $this->adapter()->copy($this->config['prefix'].'/testing_renamed.txt', $this->config['prefix'].'/testing.txt', new Config());

        $response = $this->adapter()->fileExists($this->config['prefix'].'/testing.txt');

        $this->assertTrue($response);
    }

    /**
     * @test
     */
    public function it_can_delete_a_file(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $this->adapter()->write($file, '# Testing create', new Config());

        $this->adapter()->delete($file);

        $this->assertFalse($this->adapter()->fileExists($file));
    }

    /**
     * @test
     */
    public function it_can_determine_if_a_project_has_a_file(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $this->adapter()->write($file, '# Testing create', new Config());

        $response = $this->adapter()->fileExists($this->config['prefix'].'/testing.txt');
        $this->assertTrue($response);

        $response = $this->adapter()->fileExists($this->config['prefix'].'/I_DONT_EXIST.txt');
        $this->assertFalse($response);
    }

    /**
     * @test
     */
    public function it_can_create_a_directory(): void
    {
        $this->adapter()->createDirectory($this->config['prefix'].'/aap', new Config());

        $this->assertTrue($this->adapter()->directoryExists($this->config['prefix'].'/aap'));

        $this->adapter()->deleteDirectory($this->config['prefix'].'/aap');
    }

    /**
     * @test
     */
    public function it_can_delete_a_directory(): void
    {
        $this->adapter()->createDirectory($this->config['prefix'].'/testing', new Config());
        $response = $this->adapter()->directoryExists($this->config['prefix'].'/testing');
        $this->assertTrue($response);

        $this->adapter()->deleteDirectory($this->config['prefix'].'/testing');
        $response = $this->adapter()->directoryExists($this->config['prefix'].'/testing');
        $this->assertFalse($response);
    }

    /**
     * @test
     */
    public function it_can_retrieve_a_list_of_contents_of_root(): void
    {
        $response = $this->adapter()->listContents($this->config['prefix'], 1);

        $this->assertIsArray($response);
    }

    /**
     * @test
     */
    public function it_can_retrieve_a_list_of_contents_of_root_recursive(): void
    {
        $response = $this->adapter()->listContents($this->config['prefix'], true);

        $this->assertIsArray($response);
    }

    /**
     * @test
     */
    public function it_can_retrieve_a_list_of_contents_of_sub_folder(): void
    {
        $response = $this->adapter()->listContents($this->config['prefix'].'/Forms', 1);

        $this->assertIsArray($response);
    }

    /**
     * @test
     */
    public function it_can_read_a_file(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $fileContents = '# Testing repo for `flysystem-sharepoint` project';
        $this->adapter()->write($file, $fileContents, new Config());

        $response = $this->adapter()->read($this->config['prefix'].'/testing.txt');

        $this->assertSame($fileContents, $response);
    }

    /**
     * @test
     */
    public function it_can_read_a_file_into_a_stream(): void
    {
        $file = $this->config['prefix'].'/testing.txt';
        $fileContents = '# Testing repo for `flysystem-sharepoint` project';
        $this->adapter()->write($file, $fileContents, new Config());

        $response = $this->adapter()->readStream($this->config['prefix'].'/testing.txt');

        $this->assertStringStartsWith($fileContents, stream_get_contents($response));
    }

    /**
     * @test
     */
    public function it_can_retrieve_metadata(): void
    {
        $metadata = $this->adapter()->getMetadata($this->config['prefix'].'/testing.txt');

        $this->assertInstanceOf(FileAttributes::class, $metadata);

        $this->assertSame('text/plain', $metadata->mimeType());
    }

    /**
     * @test
     */
    public function it_can_retrieve_size(): void
    {
        $metadata = $this->adapter()->fileSize($this->config['prefix'].'/testing.txt');

        $this->assertInstanceOf(FileAttributes::class, $metadata);
    }

    /**
     * @test
     */
    public function it_can_retrieve_mimetype(): void
    {
        $metadata = $this->adapter()->mimeType($this->config['prefix'].'/testing.txt');

        $this->assertInstanceOf(FileAttributes::class, $metadata);
    }

    /**
     * @test
     */
    public function it_can_retrieve_last_modified(): void
    {
        $metadata = $this->adapter()->lastModified($this->config['prefix'].'/testing.txt');

        $this->assertInstanceOf(FileAttributes::class, $metadata);
    }

    private function setInvalidClientId(): void
    {
        $client = $this->adapter()->getClient();
        $client->setClientId('123');
        $this->adapter()->setClient($client);
    }

    private function restoreClientId(): void
    {
        $client = $this->adapter()->getClient();
        $client->setClientID(static::$config[ 'client_id' ]);
        $this->adapter()->setClient($client);
    }
}
