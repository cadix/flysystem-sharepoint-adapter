<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Flysystem Adapter for SharePoint configurations
    |--------------------------------------------------------------------------
    |
    | These configurations will be used in all the tests to bootstrap
    | a Client object.
    |
     */
    'client_id'     => 'your-client-id',
    'client_secret' => 'your-client-secret',

    /**
     * Site/ teams that should be used
     */
    'branch'        => 'Documents',

    /**
     * Base URL of SharePoint server you want to use
     */
    'tenant'        => 'flysystem',
    'prefix'        => 'Shared documents',

];
