<?php

namespace Cadix\FlysystemSharePoint\Tests;

use Cadix\FlysystemSharePoint\Client;
use Cadix\FlysystemSharePoint\SharePointAdapter;
use Exception;

class SharePointAdapterDebugModeTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_retrieve_debug_mode(): void
    {
        $this->assertFalse($this->getAdapterInstance()->isDebugEnabled());
    }

    /**
     * @test
     */
    public function it_can_update_debug_mode(): void
    {
        $adapter = $this->getAdapterInstance();
        $this->assertFalse($adapter->isDebugEnabled());
        $adapter->setDebug(true);
        $this->assertTrue($adapter->isDebugEnabled());

        $adapter->setDebug(false);
        $this->assertFalse($adapter->isDebugEnabled());
    }

    /**
     * @test
     */
    public function it_can_initialize_with_debug_mode(): void
    {
        $adapter = new SharePointAdapter($this->getClientInstance(), '', true);

        $this->assertTrue($adapter->isDebugEnabled());

        $adapter = new SharePointAdapter($this->getClientInstance(), '', false);

        $this->assertFalse($adapter->isDebugEnabled());
    }

    /**
     * @test
     */
    public function it_throws_exception_in_debug_mode(): void
    {
        $client = new Client(
            $this->config['client_id'],
            $this->config[ 'client_secret' ],
            $this->config[ 'branch' ],
            $this->config[ 'tenant' ]
        );

        $adapter = new SharePointAdapter($client, '', true);

        $this->expectException(Exception::class);

        $adapter->getMetadata($this->config['prefix'].'testing.txt');
    }

    /**
     * @test
     */
    public function it_does_not_throw_exception_in_production_mode(): void
    {
        $client = new Client(
            $this->config['client_id'],
            $this->config[ 'client_secret' ],
            $this->config[ 'branch' ],
            $this->config[ 'tenant' ]
        );

        $adapter = new SharePointAdapter($client);

        $this->assertFalse($adapter->getMetadata($this->config['prefix'].'/example.txt'));
    }

    /**
     * @test
     */
    public function it_throws_exception_in_debug_mode_when_listing_contents(): void
    {
        $client = new Client(
            $this->config['client_id'],
            $this->config[ 'client_secret' ],
            $this->config[ 'branch' ],
            $this->config[ 'tenant' ]
        );

        $adapter = new SharePointAdapter($client, '', true);

        $this->expectException(Exception::class);

        $adapter->listContents('/test', 1);

        $adapter->setDebug(false);

        $this->assertSame($adapter->listContents(), []);
    }
}
