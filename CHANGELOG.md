# Changelog

All notable changes to `flysystem-sharepoint-adapter` will be documented in this file
            
## [0.2.0]()
### Added
- Added support for Laravel 9;
- Added support for PHP 8 and PHP 8.1;
- Added support for Flysystem 3;

### Changed
- Removed support for Flysystem 1 and 2;

## 0.1
- Initial release